import os
import time
from pathlib import Path
import cv2
import torch

from numpy import random

from models.experimental import attempt_load
from mc_adaptation.BasisConfiguration import YOLOConfig
from mc_adaptation.BasisDataSet import YOLODataSet
from mc_adaptation.BasisImageResult import YOLOImageResult
from utils.datasets import LoadStreams, LoadImages
from utils.general import (
    check_img_size, non_max_suppression, apply_classifier, scale_coords,
    xyxy2xywh, plot_one_box, strip_optimizer, set_logging)
from utils.torch_utils import select_device, load_classifier, time_synchronized


def detect(yolo_config: YOLOConfig):
    """
    Detect function adapted for MotionCorrect, Inc.
    Refactored from Main YoloV5 for specific usability scenario.
    :param save_img:
    :return:
    """
    device = "0"

    # Getting information from OPT setted in the outset.

    # Multiple SOURCE for data streaming.
    yolo_config.update_webcam()
    yolo_config.prepare_output_folder()

    # Initialize
    # yolo_config.get_device()
    yolo_config.set_half()

    # Attempt to load the model
    yolo_config.update_model()
    yolo_config.update_img_size()

    yolo_config.set_model_half_if_eligible()
    yolo_config.set_modelc_if_classify()

    # Set Dataset.
    yolo_config.set_dataset_img_view_save()

    # Run inference
    t0 = time.time()
    img = torch.zeros((1, 3, yolo_config.img_size, yolo_config.img_size), device=yolo_config.get_device())  # init img

    if yolo_config.get_device().type != 'cpu':
        if yolo_config.half:
            yolo_config.model(img.half())
        else:
            yolo_config.model(img)

    predict_dataset(yolo_config)

    if yolo_config.save_txt or yolo_config.save_img:
        print('Results saved to %s' % os.getcwd() + os.sep + yolo_config.out)

    print('Done. (%.3fs)' % (time.time() - t0))


def predict_dataset(yolo_config: YOLOConfig):
    """
    Take the dataset, and try to predict it by creating YOLODataset from it and
    :param yolo_config:
    :return:
    """
    # For in path, image, ? ? in the data set,
    for path, img, im0s, vid_cap in yolo_config.dataset:

        yolo_dataset = YOLODataSet(path, img, im0s, vid_cap)
        yolo_dataset.adapt_to_device(yolo_config)

        # Inference
        t1 = time_synchronized()

        # Predict
        model_predictions = yolo_config.model(yolo_dataset.img, augment=yolo_config.augment)[0]

        # Apply NMS
        model_predictions = non_max_suppression(model_predictions,
                                                yolo_config.conf_thres,
                                                yolo_config.iou_thres,
                                                classes=yolo_config.classes,
                                                agnostic=yolo_config.agnostic_nms)
        t2 = time_synchronized()

        duration = t2 - t1

        # Apply Classifier etecon TOP of the predictions
        if yolo_config.classify:
            model_predictions = apply_classifier(model_predictions, yolo_config.modelc, yolo_dataset.img, yolo_dataset.im0s)

        output_detection(yolo_config, yolo_dataset, model_predictions, duration)


def output_detection(yolo_config: YOLOConfig,
                     yolo_dataset: YOLODataSet,
                     model_predictions,
                     duration):
    """
    Go through the predictions and parse, save, stream print the results at each image level.
    :param yolo_config:
    :param yolo_dataset:
    :param model_predictions:
    :param duration:
    :return:
    """
    # Process detections
    for index, result_detection in enumerate(model_predictions):  # detections per image

        if yolo_config.webcam:  # batch_size >= 1
            yolo_image_result = YOLOImageResult(Path(yolo_dataset.path[index]),
                                                yolo_dataset.im0s[index].copy(),
                                                result_detection)
            yolo_image_result.aggregated_str_output = '%g: ' % index
        else:
            yolo_image_result = YOLOImageResult(yolo_dataset.path,
                                                yolo_dataset.im0s,
                                                result_detection)

        yolo_image_result.aggregate_output('%gx%g ' % yolo_dataset.img.shape[2:])  # print string
        yolo_image_result.update_save_path(yolo_config)

        handle_single_image_detection_result(yolo_config,
                                             yolo_dataset,
                                             yolo_image_result)

        # Print time (inference + NMS)
        print('%sDone. (%.3fs)' % (yolo_image_result.aggregated_str_output, duration))

        # Stream results
        if yolo_config.view_img:
            cv2.imshow(str(yolo_image_result.path_image), yolo_image_result.image0)
            if cv2.waitKey(1) == ord('q'):  # q to quit
                raise StopIteration

        output_images_videos(yolo_config, yolo_dataset, yolo_image_result)


def handle_single_image_detection_result(yolo_config: YOLOConfig,
                                         yolo_data: YOLODataSet,
                                         yolo_image_result: YOLOImageResult):

    if yolo_image_result.result_detection is not None and len(yolo_image_result.result_detection):
        # Rescale boxes from img_size to im0 size
        yolo_image_result.result_detection[:, :4] = scale_coords(yolo_data.img.shape[2:],
                                               yolo_image_result.result_detection[:, :4],
                                               yolo_image_result.image0.shape).round()

        aggregate_detected_classes(yolo_config, yolo_image_result)

        output_bounding_box(yolo_config,
                            yolo_image_result)


def aggregate_detected_classes(yolo_config: YOLOConfig,
                               yolo_image_result: YOLOImageResult):
    # Print class aggregated detection sum results
    for detected_class in yolo_image_result.result_detection[:, -1].unique():
        n = (yolo_image_result.result_detection[:, -1] == detected_class).sum()  # detections per class
        yolo_image_result.aggregate_output('%g %ss, ' % (n, yolo_config.get_names()[int(detected_class)]))  # add to string


def output_bounding_box(yolo_config: YOLOConfig, yolo_image_result: YOLOImageResult):
    names = yolo_config.get_names()

    # Generate randomized color for bounding boxes.
    colors = [[random.randint(0, 255) for _ in range(3)] for _ in range(len(names))]

    # Write results
    for *xyxy, confidence_detected, class_detected in yolo_image_result.result_detection:
        # Write to txt file
        if yolo_config.save_txt:


            ###############
            # Originally, Normalized, vs gain normalization to %
            # xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / yolo_image_result.gain_normalization).view(-1).tolist()

            # wtf: review this shxt. Hacking
            # Not normalized XYWH
            xywh = xyxy2xywh(torch.tensor(xyxy).view(1, 4)).view(-1).tolist()
            cxywh = list(map(int, xywh))
            #cxywh.insert(0, float(confidence_detected))

            with open(yolo_image_result.path_file_stem + '.txt', 'a') as f:
                f.write(('%g %d %d %d %d' + '\n') % (float(confidence_detected), *cxywh))  # label format

        if yolo_config.save_img or yolo_config.view_img:  # Add bbox to image
            label = '%s %.2f' % (names[int(class_detected)], confidence_detected)
            plot_one_box(xyxy, yolo_image_result.image0, label=label, color=colors[int(class_detected)], line_thickness=3)


def output_images_videos(yolo_config: YOLOConfig, yolo_dataset: YOLODataSet, yolo_result: YOLOImageResult):
    # Save results (image with detections)
    if yolo_config.save_img:
        # Save images
        if yolo_config.dataset.mode == 'images':
            cv2.imwrite(yolo_result.path_save, yolo_result.image0)

        # Save video?
        else:
            if yolo_config.vid_path != yolo_result.path_save:  # new video
                yolo_config.vid_path = yolo_result.path_save

                # There is a previously configured video writer,
                if isinstance(yolo_config.vid_writer, cv2.VideoWriter):
                    yolo_config.vid_writer.release()  # release previous video writer

                fourcc = 'mp4v'  # output video codec
                fps = yolo_dataset.vid_cap.get(cv2.CAP_PROP_FPS)
                w = int(yolo_dataset.vid_cap.get(cv2.CAP_PROP_FRAME_WIDTH))
                h = int(yolo_dataset.vid_cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                yolo_config.vid_writer = cv2.VideoWriter(yolo_result.path_save, cv2.VideoWriter_fourcc(*fourcc), fps, (w, h))
            yolo_config.vid_writer.write(yolo_result.image0)


if __name__ == '__main__':
    # Default argument parse, we are not using.
    # parser = argparse.ArgumentParser()
    # parser.add_argument('--weights', nargs='+', type=str, default='yolov5s.pt', help='model.pt path(s)')
    # parser.add_argument('--source', type=str, default='inference/images', help='source')  # file/folder, 0 for webcam
    # parser.add_argument('--output', type=str, default='inference/outpu t', help='output folder')  # output folder
    # parser.add_argument('--img-size', type=int, default=640, help='inference size (pixels)')
    # parser.add_argument('--conf-thres', type=float, default=0.4, help='object confidence threshold')
    # parser.add_argument('--iou-thres', type=float, default=0.5, help='IOU threshold for NMS')
    # parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
    # parser.add_argument('--view-img', action='store_true', help='display results')
    # parser.add_argument('--save-txt', action='store_true', help='save results to *.txt')
    # parser.add_argument('--classes', nargs='+', type=int, help='filter by class')
    # parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
    # parser.add_argument('--augment', action='store_true', help='augmented inference')
    # # opt = parser.parse_args()
    # print(opt)

    # MC Run Start:
    config_test = YOLOConfig()
    config_test.device = "0"
    config_test.source = "2"  # webcam
    #config_test.source = "/media/dyt811/A4FCE3EEFCE3B8A6/Git/MotionCorrect/SupportFiles/TestInputData/Hex201705/2017-11-12 Big and small copy pasted/"
    #config_test.source = "/home/dyt811/Git/data_mc/images/val/"
    config_test.weights = "/home/dyt811/Git/cvnnig/src/modeling/arch/yolov5/runs/exp9/weights/best.pt"
    config_test.conf_thres = 0.25

    # Prediction mode.
    with torch.no_grad():
        detect(config_test)
