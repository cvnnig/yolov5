from mc_adaptation.BasisConfiguration import YOLOConfig
import torch
from detect_mc import detect


def test_detection():
    config_test = YOLOConfig()
    config_test.device = "0"
    # config_test.source = "0"  # webcam
    config_test.source = "/media/dyt811/A4FCE3EEFCE3B8A6/Git/MotionCorrect/SupportFiles/TestInputData/Hex201705/2017-11-12 Big and small copy pasted/"
    config_test.weights = "/home/dyt811/Git/cvnnig/src/modeling/arch/yolov5/runs/exp9/weights/best.pt"
    config_test.conf_thres = 0.25

    # Prediction mode.
    with torch.no_grad():
        detect(config_test)
