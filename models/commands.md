
# Tips:
* Use  --cache-images only when ready to train. NO point when trying size.
* Multi GPU has bug.

## Train Script

### Train BS16, IMSZ640, S, 40000E
python train.py --device 0 --batch-size 16 --multi-scale --single-cls --img-size 640 --data ./data/gwd.yaml --cfg ./models/gwd_yolov5s.yaml --weights "/home/dyt811/Git/cvnnig/src/modeling/arch/yolov5/runs/exp12/weights/last.pt" --epochs 40000

### Train BS4, IMSZ1024, S, 400E
 python train.py --device 0 --batch-size 4 --multi-scale --single-cls --img-size 1024 --data ./data/gwd.yaml --cfg ./models/gwd_yolov5s.yaml --weights "" --epochs 400

### LSM Support, have to activate yolov5lsm support. Train BS4, IMSZ512, X, 400E
python train.py --device 0 --batch-size 4 --multi-scale --single-cls --img-size 512 --data ./data/gwd.yaml --cfg ./models/gwd_yolov5x.yaml --weights "" --epochs 400

### Train MotionCorrect Detector, BS16, IMSZ1024, S, 400E
CLS

### Train MotionCorrect Detector, PARALLEL mode (not working)
python -m torch.distributed.launch --nproc_per_node 2 train.py --batch-size 16 --data ./data/markers.yaml --cfg ./models/mc_yolo5s.yaml --weights "" --epochs 3000







## Detect MC Script: 

### Test YOLOV5 on latest MC environment:
time python detect_mc.py --device 0 --conf 0.1 --save-txt --weights "/home/dyt811/Git/cvnnig/src/modeling/arch/yolov5/runs/exp15/weights/best.pt" --source "/media/dyt811/A4FCE3EEFCE3B8A6/Git/MotionCorrect/SupportFiles/TestInputData/Hex201705/2017-11-12 Big and small copy pasted/"
time python detect_mc.py --device 0 --conf 0.1 --save-txt --weights "/home/dyt811/Git/cvnnig/src/modeling/arch/yolov5/runs/exp15/weights/best.pt" --source "/home/dyt811/Git/data_mc/images/val"  
  
time python detect_mc.py --device 0 --conf 0.1 --save-txt --weights "/home/dyt811/Git/cvnnig/src/modeling/arch/yolov5/runs/exp17/weights/best.pt" --source "/media/dyt811/A4FCE3EEFCE3B8A6/Git/MotionCorrect/SupportFiles/TestInputData/Hex201705/2017-11-12 Big and small copy pasted/"
time python detect_mc.py --device 0 --conf 0.1 --save-txt --weights "/home/dyt811/Git/cvnnig/src/modeling/arch/yolov5/runs/exp17/weights/best.pt" --source "/home/dyt811/Git/data_mc/images/val"





### Time Detect of BS16, IMSZ512, S, ???E
time python detect.py --device 0 --source /home/dyt811/Git/data_external_test --weights /home/dyt811/Git/yolov5/runs/Jul06_11-37-12_ArtemisIILinux_S512_BS16/best.pt --conf 0.5 --output /home/dyt811/Git/yolov5/inference/output_512predicted --img-size 512

### Time Detect of BS4, IMSZ1024, S, ???E
time python detect.py --device 0 --source /home/dyt811/Git/data_external_test --weights /home/dyt811/Git/yolov5/runs/Jul07_02-02-22_ArtemisIILinux_S1024_BS4/best.pt --conf 0.5 --output /home/dyt811/Git/yolov5/inference/output_1024predicted --img-size 1024

### Detect of BS
python detect.py --device 0 --source /home/dyt811/Git/cvnnig/data_yolo5_mc_test_large --weights /home/dyt811/Git/yolov5/runs/Jun21_11-59-00_ArtemisIILinux/best.pt --conf 0.5

### Detect Latest
python detect.py --device 0 --source /home/dyt811/Git/data_mc/images/val --weights /home/dyt811/Git/yolov5/weights/best.pt --conf 0.1

### Time Detect latest against external test sets. 
time python detect.py --device 0 --source /home/dyt811/Git/data_external_test --weights /home/dyt811/Git/yolov5/weights/best.pt --conf 0.5 --save-txt

