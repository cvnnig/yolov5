import sys, logging

logging.basicConfig(stream=sys.stderr)
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
handler_std = logging.StreamHandler()

logger.addHandler(handler_std)