from dataclasses import dataclass
from typing import Tuple
@dataclass
class YOLOV5_config_train:
    weights: str = "yolov5s.pt" # initial weights path
    cfg: str = '' #model.yaml path
    data: str = 'data/coco128.yaml' # data.yaml path
    hyp: str ='' #hyperparameters path, i.e. data/hyp.scratch.yaml
    epochs: int = 300
    batch_size: int =16 #total batch size for all GPUs
    img_size: Tuple[int, int] = (640, 640)  #train,test sizes
    rect: bool = True # rectangular training
    resume: bool = True #resume most recent training
    nosave: bool = True #only save final checkpoint
    notest: bool = True #only test final epoch
    noautoanchor: bool = True # disable autoanchor check
    evolve: bool = True # evolve hyperparameters
    bucket: str = "" #gsutil bucket
    cache_images: bool = True # cache images for faster training
    name: str = '' #renames results.txt to results_name.txt if supplied
    device: str = "0" #cuda device, i.e. 0 or 0,1,2,3 or cpu
    multi_scale: bool = True #vary img-size +/- 50%%
    single_cls: bool = True #train as single-class dataset
    adam: bool = True #use torch.optim.Adam() optimizer
    sync_bn: bool = True # use SyncBatchNorm, only available in DDP mode'
    local_rank: int = -1 # DDP parameter, do not modify
    logdir: str = 'runs/' #'logging directory
    workers: int = 8  # maximum number of dataloader workers