from pathlib import Path
import os
import shutil
#from yacs.config import CfgNode
from utils.torch_utils import select_device, load_classifier
from utils.general import check_img_size
from utils.log import logger
from utils.datasets import LoadStreams, LoadImages
from models.experimental import attempt_load
import torch
import torch.backends.cudnn as cudnn
from datetime import datetime

timestamp = datetime.now().isoformat(sep="T", timespec="auto").replace(":", "_")


class YOLOConfig:
    out: str or Path = f'inference/output_{timestamp}'
    source: str or Path = 'inference/images'  # inference size (pixels) # could be rtsp/http/txt/0 for camera
    weights: str or Path = "yolov5s.pt"
    view_img: bool = False
    save_txt: bool = True
    img_size: int = 640
    conf_thres: float = 0.4
    iou_thres: float = 0.5
    classes: list = []  # filter by class
    classify: bool = False
    device: str = ""
    half: bool = False
    webcam = None
    augment: bool = True
    agnostic_nms: bool = True
    modelc = None
    model = None
    vid_path = None
    vid_writer = None
    save_path = None
    save_img = False

    def update_webcam(self):
        # Multiple SOURCE for data streaming.
        if (self.source.isnumeric()):
            self.webcam = True
            return

        if isinstance(self.source, str) and (self.source.startswith('rtsp') or self.source.startswith('http') or self.source.endswith('.txt')):
            self.webcam = True
            return

        if isinstance(self.source, Path):
            path_stem = str(self.source)
            if path_stem.startswith('rtsp') or path_stem.startswith('http') or path_stem.endswith('.txt'):
                self.webcam = True
                return

        self.webcam = False

    def prepare_output_folder(self):
        out = self.out
        if os.path.exists(out):
            shutil.rmtree(out)  # delete output folder
        os.makedirs(out)  # make new output folder

    def get_device(self) -> torch.device:
        # Dynamically Initialize the Torch.Device
        # Note that torch.device type is not YACS serializable and hence must be dynamically generated and returned.
        return select_device(self.device)

    def set_half(self):
        # half precision only supported on CUDA
        self.half = self.get_device().type != 'cpu'

    def update_model(self):
        # Attempt to load the model
        self.model = attempt_load(str(self.weights), map_location=self.get_device())  # load FP32 model

    def update_img_size(self):
        self.img_size = check_img_size(self.img_size, s=self.model.stride.max())  # check img_size

    def set_model_half_if_eligible(self):
        try:
            assert self.half is not None
            assert self.model is not None
            # Turn on half precision mode.
            if self.half:
                self.model.half()  # to FP16
        except AssertionError:
            logger.error("Either Half or Model is not set in the first place. ")

    def set_modelc_if_classify(self):
        # Second-stage classifier using another classifier to determine the class?
        # Disabled by DEFAULT
        if self.classify:
            self.modelc = self.set_classification_model()

    def set_classification_model(self):
        self.modelc = load_classifier(name='resnet101', n=2)  # initialize
        self.modelc.load_state_dict(
            torch.load('weights/resnet101.pt', map_location=self.get_device())['model'])  # load weights
        self.modelc.to(self.get_device()).eval()

    def set_dataset_img_view_save(self):
        if self.webcam:
            self.view_img = True
            cudnn.benchmark = True  # set True to speed up constant image size inference
            self.dataset = LoadStreams(str(self.source), img_size=self.img_size)
        else:
            self.save_img = True
            self.dataset = LoadImages(str(self.source), img_size=self.img_size)

    def get_names(self):
        if hasattr(self.model, 'module'):
            return self.model.module.names
        else:
            return self.model.names