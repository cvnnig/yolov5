from dataclasses import dataclass
from pathlib import Path
import numpy as np
import torch
from mc_adaptation.BasisConfiguration import YOLOConfig
from mc_adaptation.BasisDataSet import YOLODataSet

@dataclass
class YOLOImageResult:

    path_image: Path
    image0: np.array
    gain_normalization: np.array
    aggregated_str_output: str
     # string summary of the fresults findings

    def __init__(self, path_image: str or Path, image0, result_detection):
        self.path_image = Path(path_image)
        self.image0 = image0
        self.result_detection = result_detection
        self.gain_normalization = torch.tensor(self.image0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
        self.aggregated_str_output: str = ""

    def aggregate_output(self, str_input):
        self.aggregated_str_output += str_input

    def update_save_path(self, yolo_config: YOLOConfig):
        self.path_save = str(Path(yolo_config.out) / self.path_image.name)
        self.path_file_stem = str(Path(yolo_config.out) / self.path_image.stem) + (
            '_%g' % yolo_config.dataset.frame if yolo_config.dataset.mode == 'video' else '')
