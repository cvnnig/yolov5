from mc_adaptation.BasisConfiguration import YOLOConfig
import torch

class YOLODataSet:
    def __init__(self, path, img, im0s, vid_cap):
        self.path = path
        self.img = img
        self.im0s = im0s
        self.vid_cap = vid_cap

    def adapt_to_device(self, yolo_config: YOLOConfig):
        # Send to Cuda,
        self.img = torch.from_numpy(self.img).to(yolo_config.get_device())

        # Set to half or else use float.
        self.img = self.img.half() if yolo_config.half else self.img.float()  # uint8 to fp16/32

        # image = image / 255, normalize to be under 1: # 0 - 255 to 0.0 - 1.0
        self.img /= 255.0

        # padding?
        if self.img.ndimension() == 3:
            self.img = self.img.unsqueeze(0)

